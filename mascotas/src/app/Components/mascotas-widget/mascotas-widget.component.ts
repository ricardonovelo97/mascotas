import { Component, OnInit } from '@angular/core';
import { Interface } from 'readline';

interface IDataResponse{
  mascota_url: string;
}

interface IApiResponse{
  message: string;
  status: string;
}

@Component({
  selector: 'app-mascotas-widget',
  templateUrl: './mascotas-widget.component.html',
  styleUrls: ['./mascotas-widget.component.scss'],
})
export class MascotasWidgetComponent implements OnInit {

  MascotaData: IDataResponse;
  constructor() { }

  ngOnInit() {
    this.MascotaData = <IDataResponse>{};
    this.getMascotaData();
  }

  getMascotaData(){
    fetch(`https://dog.ceo/api/breeds/image/random`)
    .then(response => response.json())
    .then((data: IApiResponse) => {
    this.MascotaData.mascota_url = data.message;
    });
  }

}
